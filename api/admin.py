from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import *

from copy import deepcopy

# Register your models here.
cloneUserAdminFieldSet = deepcopy(UserAdmin.fieldsets)
cloneUserAdminFieldSet[1][1]["fields"] = cloneUserAdminFieldSet[1][1]["fields"] + \
                                         ('pays', 'nationalite', 'adresse', 'station')
cloneUserAdminFieldSet[2][1]["fields"] = cloneUserAdminFieldSet[2][1]["fields"][:3]

userAdminFieldsetCustom = (
    cloneUserAdminFieldSet[1],
    cloneUserAdminFieldSet[0],
    cloneUserAdminFieldSet[3],
    cloneUserAdminFieldSet[2]
)


class PompesOfStationInline(admin.TabularInline):
    model = Pompe


class LignesCommandesOfCommandeInline(admin.TabularInline):
    model = LigneCommande


@admin.register(Utilisateur)
class UtilisateurAdmin(UserAdmin):
    list_display = ('id', 'username', 'email', 'first_name', 'last_name',
                    'pays', 'station', 'is_active', 'is_superuser')
    search_fields = ('username', 'email', 'first_name')
    list_filter = ('username', "first_name", 'is_active', "is_superuser")
    fieldsets = userAdminFieldsetCustom
    add_fieldsets = UserAdmin.add_fieldsets + (
        (None, {'fields': ('pays', 'nationalite', 'adresse', 'station')}),
    )


@admin.register(Fournisseur)
class FournisseurAdmin(admin.ModelAdmin):
    list_display = ('nom', 'adresse')


@admin.register(Station)
class StationAdmin(admin.ModelAdmin):
    list_display = ('nom', 'adresse')
    # search_fields = ('username', 'email', 'first_name')
    # list_filter = ('username', "first_name", 'is_active', "is_superuser")
    # fieldsets = userAdminFieldsetCustom
    # add_fieldsets = UserAdmin.add_fieldsets + (
    #     (None, {'fields': ('pays', 'nationalite', 'adresse')}),
    inlines = [
        PompesOfStationInline,
    ]


@admin.register(Pompe)
class PompeAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'libelle')


@admin.register(TypeCarburant)
class TypeCarburantAdmin(admin.ModelAdmin):
    list_display = ('libelle', 'prix_vente', 'prix_vente')


@admin.register(Reservoir)
class ReservoirAdmin(admin.ModelAdmin):
    list_display = ('libelle', 'volume')


@admin.register(Index)
class IndexAdmin(admin.ModelAdmin):
    list_display = ('numero', 'volume_vendu', 'description')


@admin.register(Commande)
class CommandeAdmin(admin.ModelAdmin):
    readonly_fields = ('volume_commande', 'volume_livre', 'prix_total')
    list_display = ('station', 'fournisseur', 'date_commande', 'date_livraison', 'volume_commande', 'volume_livre', 'prix_total',
                    'statut')
    inlines = [
        LignesCommandesOfCommandeInline,
    ]


@admin.register(LigneCommande)
class LigneCommandeAdmin(admin.ModelAdmin):
    list_display = ("libelle", 'prix_litre', 'type_carburant', 'commande', 'prix_litre')
