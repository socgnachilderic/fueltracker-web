from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models


# Create your models here.


class UtilisateurManager(BaseUserManager):
    def create_user(self, username, email, password=None):
        if not username:
            raise ValueError("L'utilisateur doit avoir un nom d'utilisateur")
        if not email:
            raise ValueError("L'utilisateur doit avoir un email")
        user = self.model(username=username, email=self.normalize_email(email))
        user.set_password(password)
        user.is_active = True
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password):
        user = self.create_user(username, email, password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Fournisseur(models.Model):
    """Classe définissant la table Fournisseurs en Base de données

    :param nom:                 Le nom du fournisseur
    :param adresse:             L'adresse du fournisseur
    """
    nom = models.CharField(max_length=100)
    adresse = models.CharField(max_length=200)

    def __str__(self):
        return self.nom


class Station(models.Model):
    """Classe définissant la table Station service en Base de données

    :param nom: Le nom de la station service
    :param adresse:             L'adresse de la station
    :param utilisateurs:        Les utilisateurs associés à des stations
    :param fournisseurs:        Les fournisseurs associés à des stations
    """
    nom = models.CharField(max_length=100)
    adresse = models.CharField(max_length=200)
    fournisseurs = models.ManyToManyField(Fournisseur, through="Commande")

    def __str__(self):
        return self.nom


class Utilisateur(AbstractUser):
    """Classe définissant la table Utilisateur en Base de données

    Elle héritet de la classe AbstractUser qui par défaut a les champs: (first_name, last_name, is_active, group etc...)

    :param pays:                Le pays de l'utilisateur
    :param nationalite:         La nationalité de l'utilisateur
    :param adresse:             L'adresse de l'utilisateur
    """
    pays = models.CharField(max_length=100)
    nationalite = models.CharField(max_length=100)
    adresse = models.CharField(max_length=200)
    station = models.ForeignKey(Station, on_delete=models.CASCADE, null=True)

    objects = UtilisateurManager()

    def __str__(self):
        return self.username


class Pompe(models.Model):
    """Classe définissant la table Pompe en Base de données

    :param code:                Le code de la pompe
    :param libelle:             Le libellé de la pompe
    :param station:             Les pompes d'une certaine station
    """
    code = models.CharField(max_length=50)
    libelle = models.CharField(verbose_name="Libellé", max_length=200)
    station = models.ForeignKey(Station, on_delete=models.CASCADE)

    def __str__(self):
        return self.libelle


class TypeCarburant(models.Model):
    """Classe définissant la table TypeCarburant en Base de données

    :param libelle:             Le libellé du type carburant
    :param prix_vente:          Le prix de vente d'un litre de ce type de carburant
    :param prix_achat:          Le prix de achat d'un litre de ce type de carburant
    """
    libelle = models.CharField(verbose_name="Libellé", max_length=200)
    prix_vente = models.FloatField(verbose_name="Prix de vente")
    prix_achat = models.FloatField(verbose_name="Prix d'achat")
    station = models.ForeignKey(Station, on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name = "Type de carburant"

    def __str__(self):
        return self.libelle


class Reservoir(models.Model):
    """Classe définissant la table Reservoir en Base de données

    :param libelle:             Le libellé du reservoir
    :param volume:              Le volume du reservoir
    :param station:             Les reservoirs liés à un type de carburant
    """
    libelle = models.CharField(verbose_name="Libellé", max_length=200)
    volume = models.FloatField()
    station = models.ForeignKey(Station, on_delete=models.CASCADE, null=True)
    type_carburant = models.ForeignKey(TypeCarburant, verbose_name="Type de carbuant", on_delete=models.CASCADE)

    def __str__(self):
        return self.libelle


class Index(models.Model):
    """Classe définissant la table Index en Base de données

    Il s'agit des différents index d'une pompe
    :param numero:              Le numéro d'un index
    :param volume_vendu:        Le volume de carburant vendu par le pistolet d'une pompe
    :param description:         La description d'un index
    """
    numero = models.BigIntegerField(verbose_name="Numéro")
    volume_vendu = models.FloatField()
    description = models.TextField(null=True, blank=True)
    pompe = models.ForeignKey(Pompe, on_delete=models.CASCADE)
    reservoir = models.ForeignKey(Reservoir, on_delete=models.CASCADE)

    def __str__(self):
        return self.numero


class Commande(models.Model):
    """Classe définissant la table Commande en Base de données

    :param date_commande:       La date de la commande
    :param date_livraison:      La date de livraison de la commande
    :param station:             Les reservoirs liés à un type de carburant
    :param volume_commande:     Volume de carburant commandé
    :param volume_livre:        Volume de carburant livré
    :param prix_total:          prix total de la commande
    :param statut:              statut de la commande en un instant t
    """
    LISTE_STATUT = (
        ('EN', "enregistrer"),
        ('CO', "commander"),
        ('LI', "livrer")
    )
    station = models.ForeignKey(Station, on_delete=models.CASCADE)
    fournisseur = models.ForeignKey(Fournisseur, on_delete=models.CASCADE)
    statut = models.CharField(max_length=2, choices=LISTE_STATUT, blank=True, default="EN")
    date_commande = models.DateField(verbose_name="Date de la commande", auto_now_add=True)
    ristourne = models.FloatField(default=0, blank=True)
    date_livraison = models.DateField(verbose_name="Date de livraison", blank=True, null=True)

    @property
    def volume_commande(self):
        volume: float = 0
        for commande in self.get_lignes_commandes():
            volume += commande.volume_commande
        return volume

    @property
    def volume_livre(self):
        volume: float = 0
        for commande in self.get_lignes_commandes():
            volume += commande.volume_livre
        return volume

    @property
    def prix_total(self):
        prix: float = 0
        for commande in self.get_lignes_commandes():
            prix += commande.volume_livre * commande.prix_litre
        return prix

    def get_lignes_commandes(self) -> iter:
        return LigneCommande.objects.filter(commande=self)

    def __str__(self):
        return f"commande N°{self.pk}"


class LigneCommande(models.Model):
    """Classe définissant la table LigneCommande en Base de données

    :param libelle:             Le libellé de la ligne de commande
    :param volume:              Le volume total d'une ligne de commande
    :param prix_litre:          Le prix du litre de commande
    """
    libelle = models.CharField(verbose_name="Libellé", max_length=200)
    volume_commande = models.FloatField(verbose_name="Volume commandé", default=0)
    volume_livre = models.FloatField(verbose_name="Volume livré", default=0, blank=True)
    prix_litre = models.BigIntegerField(verbose_name="Prix du litre")
    type_carburant = models.ForeignKey(TypeCarburant, verbose_name="Type de carburant", on_delete=models.CASCADE)
    commande = models.ForeignKey(Commande, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Ligne de commande"

    def __str__(self):
        return self.libelle
