from django.test import TestCase
from api.models import *

# Create your tests here.


class UtilisateurModelTest(TestCase):
    """Test du model Utilisateur

    """
    @classmethod
    def setUpTestData(cls):
        Utilisateur.objects.create(
            username="westbook",
            email="westbook90@gmail.com",
            first_name="SOCGNA KOUYEM",
            last_name="Childéric",
            password="Socgna 2000",
            pays="Cameroun",
            nationalite="Camerounaise",
            adresse="ydé 35342 Nsimeyong"
        )

    def test_get_utilisateur(self):
        utilisateur = Utilisateur.objects.get(username="westbook")
        print(utilisateur)
        self.assertEqual(utilisateur.email, "westbook90@gmail.com")


# class FournisseurModelTest(TestCase):
#     pass
#
#
# class StationModelTest(TestCase):
#     pass
#
#
# class PompeModelTest(TestCase):
#     pass
#
#
# class TypeCarburantModelTest(TestCase):
#     pass
#
#
# class ReservoirModelTest(TestCase):
#     pass
#
#
# class IndexModelTest(TestCase):
#     pass
#
#
# class CommandeModelTest(TestCase):
#     pass
#
#
# class LigneCommandeModelTest(TestCase):
#     pass
