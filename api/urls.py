from django.urls import path, include
from rest_framework import routers
from rest_framework_simplejwt import views as jwt_views
from .views import *

router = routers.DefaultRouter()
router.register(r"utilisateurs", UtilisateurViewSet)
router.register(r"fournisseurs", StationViewSet)
router.register(r"stations", StationViewSet)
router.register(r"pompes", StationViewSet)
router.register(r"types-carburants", StationViewSet)
router.register(r"reservoirs", StationViewSet)
router.register(r"index", StationViewSet)
router.register(r"commandes", StationViewSet)
router.register(r"lignes-commandes", StationViewSet)


urlpatterns = [
    path('token/obtain/', jwt_views.TokenObtainPairView.as_view(), name='token_create'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('blacklist/', LogoutAndBlacklistRefreshTokenForUserView.as_view(), name="blacklist"),

    path('', include(router.urls)),
    path('current-user/', current_user, name='current_user')
]
