from rest_framework import viewsets, views
from rest_framework import status, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework_simplejwt.tokens import RefreshToken
import json

from .models import *
from .serializers import *


# Create your views here.

@api_view(['GET'])
def current_user(request):
    user = Utilisateur.objects.filter(pk=request.user.id)
    serializer = UtilisateurSerializer(user, many=True)
    return Response(serializer.data)


class LogoutAndBlacklistRefreshTokenForUserView(APIView):
    permission_classes = (permissions.AllowAny,)
    authentication_classes = ()

    def post(self, request):
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist()
            return Response(status=status.HTTP_205_RESET_CONTENT)
        except Exception:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class UtilisateurViewSet(viewsets.ModelViewSet):
    queryset = Utilisateur.objects.filter(is_active=True)
    serializer_class = UtilisateurSerializer

    def get_queryset(self):
        user = Utilisateur.objects.get(pk=self.request.user.id)
        return Utilisateur.objects.filter(is_active=True, station=user.station)


class FournisseurViewSet(viewsets.ModelViewSet):
    queryset = Fournisseur.objects.all()
    serializer_class = FournisseurSerializer


class StationViewSet(viewsets.ModelViewSet):
    queryset = Station.objects.all()
    serializer_class = StationSerializer


class PompeViewSet(viewsets.ModelViewSet):
    queryset = Pompe.objects.all()
    serializer_class = PompeSerializer


class TypeCarburantViewSet(viewsets.ModelViewSet):
    queryset = TypeCarburant.objects.all()
    serializer_class = TypeCarburantSerializer


class ReservoirViewSet(viewsets.ModelViewSet):
    queryset = Reservoir.objects.all()
    serializer_class = ReservoirSerializer


class IndexViewSet(viewsets.ModelViewSet):
    queryset = Index.objects.all()
    serializer_class = IndexSerializer


class CommandeViewSet(viewsets.ModelViewSet):
    queryset = Commande.objects.all()
    serializer_class = CommandeSerializer


class LigneCommandeViewSet(viewsets.ModelViewSet):
    queryset = LigneCommande.objects.all()
    serializer_class = LigneCommandeSerializer
