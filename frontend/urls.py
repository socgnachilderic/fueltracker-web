from django.urls import path, include

from . import views

app_name = 'fuel'

urlpatterns = [
    path('', views.HomeView.as_view()),
    path('home/', views.HomeView.as_view(), name="home"),
    path('login/', views.LoginView.as_view(), name="login"),
    path('logout/', views.LogoutView.as_view(), name="logout"),
    path('profile/', views.ProfileView.as_view(), name="profile"),
    path('settings/', include([
        path('station/', views.setting_station_view, name="setting_station"),
        path('fournisseurs/', views.setting_fournisseurs_view, name="setting_fournisseurs"),
        path('reservoirs/', views.setting_reservoirs_view, name="setting_reservoirs"),
    ]))
]
