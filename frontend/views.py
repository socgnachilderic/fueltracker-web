from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import generic
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from api import models

# Create your views here.


class LoginView(generic.TemplateView):
    template_name = "pages/login.html"
    error = False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["error"] = self.error
        return context

    def post(self, request, **kwargs):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None and user.is_active:
            login(request, user)
            return redirect("fuel:home")
        self.error = True
        return self.get(request, **kwargs)


class LogoutView(generic.View):
    @staticmethod
    def get(request, **kwargs):
        logout(request)
        return redirect("fuel:login")


@method_decorator(login_required, name="dispatch")
class HomeView(generic.TemplateView):
    template_name = "pages/index.html"


@method_decorator(login_required, name="dispatch")
class ProfileView(generic.UpdateView):
    template_name = "pages/profile.html"
    fields = ['last_name', 'first_name', 'email', 'pays', 'nationalite', 'adresse', 'username']

    def get_object(self, queryset=None):
        return models.Utilisateur.objects.get(pk=self.request.user.pk)

    def get_success_url(self):
        password = self.request.POST["password"]
        user = self.get_object()
        if user.check_password(password):
            password = self.request.POST["new_password"]
            user.set_password(password)
            user.save()
            update_session_auth_hash(self.request, user)
        return reverse('fuel:profile')


@login_required
def setting_station_view(request):
    station = models.Station.objects.get(utilisateur__username=request.user.username)
    type_carburants = models.TypeCarburant.objects.filter(station=station)
    return render(request, "pages/settings/station.html", locals())


@login_required
def setting_fournisseurs_view(request):
    fournisseurs = models.Fournisseur.objects.filter(station__utilisateur__username=request.user.username)
    return render(request, "pages/settings/fournisseurs.html", locals())


@login_required
def setting_reservoirs_view(request):
    station = models.Station.objects.get(utilisateur__username=request.user.username)
    pompes = models.Pompe.objects.filter(station=station)
    indexs = models.Index.objects.filter(pompe__station=station)
    reservoirs = models.Reservoir.objects.filter(station=station)
    print(pompes, indexs, reservoirs)
    return render(request, "pages/settings/reservoirs.html")
